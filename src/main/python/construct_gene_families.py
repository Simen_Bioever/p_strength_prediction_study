### DIVIDE GENES INTO GENE FAMILIES ###
### Part2: USE PYTHON TO CONSTRUCT GENE FAMILIES 
### FIRST MAKE SURE THAT "markov_clustering" AND "networkx" HAVE BEEN INSTALLED 
### THE "markov_clustering" PACKAGE IS ONLY COMPATIBLE WITH PYTHON 3 !!!!
### PART1 is in Generate_paralogous_gene_pairs.R

import pandas as pd
import markov_clustering as mc
import networkx as nx
import numpy as np

data=pd.read_csv('result_V3_to_V3_filtered',sep='\t')
edges_with_weights=[(data['qgene'][i],data['sgene'][i],data["bitscore"][i]) for i in range(len(data))]
G=nx.Graph()
G.add_weighted_edges_from(edges_with_weights)
matrix=nx.to_scipy_sparse_matrix(G)

result11=mc.run_mcl(matrix,inflation=1.1)
result15=mc.run_mcl(matrix,inflation=1.5)
result20=mc.run_mcl(matrix,inflation=2)

clusters11=mc.get_clusters(result11)
clusters15=mc.get_clusters(result15)
clusters20=mc.get_clusters(result20)

nodes=G.nodes()

gene_families11=np.array([[nodes[element] for element in tup] for tup in clusters11])
gene_families15=np.array([[nodes[element] for element in tup] for tup in clusters15])
gene_families20=np.array([[nodes[element] for element in tup] for tup in clusters20])

np.save(file='gene_families11',arr=gene_families11)
np.save(file='gene_families15',arr=gene_families15)
np.save(file='gene_families20',arr=gene_families20)

# The file "gene_families11.npy" is chosen for downstream analysis
# Result can be later flatterned by [item for items in gene_families for item in items]


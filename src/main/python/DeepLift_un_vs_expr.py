import matplotlib
matplotlib.use('Agg')
import pandas as pd
import Bio.SeqIO
import numpy as np
import math
import random
import pickle
import deeplift
from keras.utils import np_utils
from keras import backend as K
from keras import models
from keras import activations
from itertools import izip
from sklearn.metrics import confusion_matrix
from time import time
from sys import argv
from deeplift.conversion import kerasapi_conversion as kc
from deeplift.dinuc_shuffle import dinuc_shuffle
import os.path

DIVISIONS=["subsample_assignment"+str(i) for i in range(1,11)]
SUBSAMPLES=["subsample"+str(i) for i in range(1,6)]
PREDICTOR='Pro_and_Ter'

#set source of input data
SEQ_FILE_PRO='../../1_sequences_and_data_splitting/promoters.fa'
SEQ_FILE_TER='../../1_sequences_and_data_splitting/terminators.fa'
DATA_FILE='../../1_sequences_and_data_splitting/un_vs_expr.csv'
SUMMARY_DIRECTORY='../../8_trained_models_un_vs_expr_remove_codon/'
SUMMARY_FILE='SUMMARY_FILE_un_vs_expr_remove_codon'
data=pd.read_csv(DATA_FILE,sep='\t')
data=data.set_index('Geneid')

def run_deeplift():

 geneIDs=[]
 categories=[]
 seqs=[]
 seqs_pro=[]
 seqs_ter=[]
 seqs_shuffled=[]
 seqs_pro_shuffled=[]
 seqs_ter_shuffled=[]

 # read sequences 
 for x,y in izip(Bio.SeqIO.parse(SEQ_FILE_PRO,'fasta'), Bio.SeqIO.parse(SEQ_FILE_TER,'fasta') ):
     geneID=x.id

     seq_pro=x.seq.tostring()
     seq_pro_shuffled=dinuc_shuffle(seq_pro)

     seq_ter=y.seq.tostring()
     seq_ter_shuffled=dinuc_shuffle(seq_ter)

     seq=seq_pro+seq_ter
     seq_shuffled=seq_pro_shuffled+seq_ter_shuffled

     if geneID in data.index:
         seqs.append(seq)
         seqs_shuffled.append(seq_shuffled)

         seqs_pro.append(seq_pro)
         seqs_pro_shuffled.append(seq_pro_shuffled)

         seqs_ter.append(seq_ter)
         seqs_ter_shuffled.append(seq_ter_shuffled)

         geneIDs.append(geneID)
         category=data['category'][geneID]
         if category=='expressed':
              categories.append(0)
         else:
              categories.append(1)

 geneIDs=np.array(geneIDs)
 categories=np_utils.to_categorical(np.array(categories),2)
         
 # one-hot encoding
 dict={'A':[1,0,0,0],'C':[0,1,0,0],'G':[0,0,1,0],'T':[0,0,0,1]}

 def one_hot_encoding(seq):
    one_hot_encoded=np.zeros(shape=(4,len(seq)))
    for i,nt in enumerate(seq):
        one_hot_encoded[:,i]=dict[nt]
    return one_hot_encoded

 print "ONE-HOT ENCODING STARTED"
 if PREDICTOR=="Pro_and_Ter":
    one_hot_seqs=np.expand_dims(np.array([ one_hot_encoding(seq) for seq in seqs],dtype=np.float32),3)
    one_hot_seqs_shuffled=np.expand_dims(np.array([ one_hot_encoding(seq) for seq in seqs_shuffled],dtype=np.float32),3)
    one_hot_seqs[:,:,1000:1003,:]=0
    one_hot_seqs[:,:,1997:2000,:]=0
    one_hot_seqs_shuffled[:,:,1000:1003,:]=0
    one_hot_seqs_shuffled[:,:,1997:2000,:]=0
 if PREDICTOR=="Pro":
    one_hot_seqs=np.expand_dims(np.array([ one_hot_encoding(seq) for seq in seqs_pro],dtype=np.float32),3)
    one_hot_seqs_shuffled=np.expand_dims(np.array([ one_hot_encoding(seq) for seq in seqs_pro_shuffled],dtype=np.float32),3)
    one_hot_seqs[:,:,1000:1003,:]=0
    one_hot_seqs_shuffled[:,:,1000:1003,:]=0
 if PREDICTOR=='Ter':
    one_hot_seqs=np.expand_dims(np.array([ one_hot_encoding(seq) for seq in seqs_ter],dtype=np.float32),3)
    one_hot_seqs_shuffled=np.expand_dims(np.array([ one_hot_encoding(seq) for seq in seqs_ter_shuffled],dtype=np.float32),3)
    one_hot_seqs[:,:,497:500,:]=0
    one_hot_seqs_shuffled[:,:,497:500,:]=0

 print "ONE-HOT ENCODING FINISHED"

 un_vs_expr_summary=pd.read_csv(SUMMARY_DIRECTORY+SUMMARY_FILE,sep='\t',header=None)
 un_vs_expr_summary.columns=['name','splitting_id','testing_subsample','shuffle','tp','tn','fp','fn','accuracy','test_total','predictor']

 all_scores_tp=[]
 all_scores_tn=[]

 for DIVISION in DIVISIONS:
   for SUBSAMPLE in SUBSAMPLES:
      row_indices=[ i for i in range(un_vs_expr_summary.shape[0]) if (un_vs_expr_summary['shuffle'][i]=='None' and un_vs_expr_summary['splitting_id'][i]==DIVISION and un_vs_expr_summary['testing_subsample'][i]==SUBSAMPLE) and un_vs_expr_summary['predictor'][i]==PREDICTOR ]
      for row_index in row_indices:
          name=un_vs_expr_summary['name'][row_index]
          splitting_id=un_vs_expr_summary['splitting_id'][row_index]
          testing_subsample=un_vs_expr_summary['testing_subsample'][row_index]
          predictor=un_vs_expr_summary['predictor'][row_index]

          testing_genes=np.array([gene for gene in geneIDs if data[splitting_id][gene]==testing_subsample])
          testing_indices=np.array([np.where(geneIDs==gene)[0][0] for gene in testing_genes])
          testing_data=one_hot_seqs[testing_indices]
          testing_data_shuffled=one_hot_seqs_shuffled[testing_indices]          

          testing_categories=np.argmax(categories[testing_indices],axis=1)
          # load model and make predictions
          model=models.load_model(SUMMARY_DIRECTORY+"MODEL_"+str(name))
          prediction=np.argmax(model.predict(testing_data),axis=1)
    
          deeplift_model =\
              kc.convert_model_from_saved_files(
              SUMMARY_DIRECTORY+"MODEL_"+str(name),
              nonlinear_mxts_mode=deeplift.layers.NonlinearMxtsMode.DeepLIFT_GenomicsDefault)

          deeplift_contribs_func = deeplift_model.get_target_contribs_func(
                            find_scores_layer_idx=0,
                            target_layer_idx=-2)

          # calculate deeplift for each gene (only true positives and true negatives)
          tp_data=[]
          tn_data=[]
          tp_data_shuffled=[]
          tn_data_shuffled=[]
          for i,j in enumerate(testing_indices):
              testing_gene=geneIDs[j]
              if testing_categories[i]==1 and prediction[i]==1:
                  tp_data.append(testing_data[i])   
                  tp_data_shuffled.append(testing_data_shuffled[i])
              elif testing_categories[i]==0 and prediction[i]==0:
                  tn_data.append(testing_data[i])
                  tn_data_shuffled.append(testing_data_shuffled[i])
          tp_data=np.array(tp_data)
          tn_data=np.array(tn_data)
          tp_data_shuffled=np.array(tp_data_shuffled)
          tn_data_shuffled=np.array(tn_data_shuffled)

          scores_tp = np.array(deeplift_contribs_func(task_idx=1,input_data_list=[tp_data],input_references_list=[tp_data_shuffled],batch_size=10,progress_update=1000))
          scores_tn = np.array(deeplift_contribs_func(task_idx=0,input_data_list=[tn_data],input_references_list=[tn_data_shuffled],batch_size=10,progress_update=1000))
          print scores_tp.shape          
          print scores_tn.shape
          averaged_scores_tp = np.squeeze(np.average(scores_tp,axis=0))
          averaged_scores_tn = np.squeeze(np.average(scores_tn,axis=0))
          all_scores_tp.append(averaged_scores_tp)
          all_scores_tn.append(averaged_scores_tn)
          print DIVISION,SUBSAMPLE

 all_scores_tp_averaged=np.average(np.array(all_scores_tp),axis=0)
 all_scores_tn_averaged=np.average(np.array(all_scores_tn),axis=0)
 return (all_scores_tp_averaged,all_scores_tn_averaged)

tps=[]
tns=[]
for i in range(10):
 tp,tn=run_deeplift() 
 tps.append(tp)
 tns.append(tn)
tps_averaged=np.average(np.array(tps),axis=0)
tns_averaged=np.average(np.array(tns),axis=0)

pd.DataFrame(tps_averaged).to_csv('un_vs_high.tp.csv')
pd.DataFrame(tns_averaged).to_csv('un_vs_high.tn.csv')



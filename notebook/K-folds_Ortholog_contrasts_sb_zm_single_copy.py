
# coding: utf-8

# In[1]:


#import Ortholog_contrasts_Apr2018_functions_only as OC
import numpy as np
import pandas as pd
import random
import sys, os


# In[2]:


def get_process_gene_fams():
    gene_families = np.load("../data/contrast_data/gene_families.npy")
    #Convert to pandas data frame for easier use
    gfL=[]
    gL=[]
    for gf in range(0,len(gene_families)):
        for gene in gene_families[gf]:
            gfL.append(gf)
            gL.append(gene)
    gene_fams = pd.DataFrame({"gene_family":gfL,"gene_id":gL})
    return gene_fams


# In[3]:


def k_fold_split_by_gene_family(genefams,fold_num):
    folds=[]
    fams = gene_fams["gene_family"].unique().tolist()
    random.shuffle(fams)
    fams_per_fold=len(fams)/fold_num
    genes_per_fold=[]
    for fold in range(0,fold_num):
        #print fold*fams_per_fold, (fold+1)*fams_per_fold
        folds.append(genefams[genefams["gene_family"].isin(fams[fold*fams_per_fold:(fold+1)*fams_per_fold])])
        genes_per_fold.append(len(folds[fold]))
    #print np.array(genes_per_fold).mean()
    #print np.array(genes_per_fold).std()
    return folds, genes_per_fold


# In[4]:


gene_fams = get_process_gene_fams()


# In[5]:


random.seed(5418740)
folds, genes_per_fold = k_fold_split_by_gene_family(gene_fams,5)
print np.array(genes_per_fold).std()
print genes_per_fold


# In[7]:


for num in range(0,5):
    #if num!=0:continue
    #print num
    test_set = folds[num].copy()
    #print [x for x in range(0,5) if x != num]
    train_set = pd.concat([folds[x].copy() for x in range(0,5) if x != num])
    #sanity check that test_set does not overlap with training set
    if len(test_set['gene_id'].unique())!=len(test_set['gene_id']):
        print "WARNING: DUPLICATES IN TEST SET, SPLITTING NOT WORKING CORRECTLY."
    if [x for x in test_set['gene_id'].tolist() if x in train_set['gene_id'].tolist()] != []:
        print "WARNING: TRAINING TESTING SET CONTAMINATION. DO NOT PROCCED."
    test_set.to_csv("../data/contrast_data/TMP_TEST_SET"+str(num)+".csv")
    train_set.to_csv("../data/contrast_data/TMP_TRAIN_SET"+str(num)+".csv")
    for rep in range(0,10):
        #if rep!=0:continue
        cmd = "python run_ortholog_contrast_fold_sb_zm_single_copy_Conv2D.py "+str(num)+" "+str(rep)
        print cmd
        os.system(cmd)
        #re-run if model did not train at all...
        mod_stats = pd.read_csv("../figs/All_model_stats_sb_zm_sum_C2D_10reps.txt")
        while float(mod_stats[mod_stats["tissues"].str[-9:]=="rep"+str(rep)+"fold"+str(num)]["acc_trn"].values[-1]) < 0.55:
            print "model did not train, re-running..."
            cmd = "python run_ortholog_contrast_fold_sb_zm_single_copy_Conv2D.py "+str(num)+" "+str(rep)
            print cmd
            os.system(cmd)
            mod_stats = pd.read_csv("../figs/All_model_stats_sb_zm_sum_C2D_10reps.txt")

